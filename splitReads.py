import globalVars


# small class defining the content of fastq file
class myFQ:
    def __init__(self, iD, seq, opt, qual):
        self.iD = iD
        self.seq = seq
        self.opt = opt
        self.qual = qual

# small class defining the content of fasta file
class myFA:
    def __init__(self, iD, seq):
        self.iD = iD
        self.seq = seq


#function to split reads in fastq format
def splitReadsFQ(inFQ, oFQ, splitSize):

    if inFQ.endswith(".gz"):
        import gzip
        fqParser = gzip.open(inFQ, mode='rt')
    else:
        fqParser = open(inFQ)

    myWriter = open(oFQ, "w")

    lineCount = 0
    seqCount = 0
    writtenREAD_ID = 1
    for line in fqParser:
        lineCount += 1

        if lineCount == 1:
            rid=line[1:].rstrip().split("\t")[0].split(" ")[0]
            curRead = myFQ(line.strip(), "", "", "")
        elif lineCount == 2:
            curRead.seq = line.strip()
        elif lineCount == 3:
            curRead.opt = line.strip()
        elif lineCount == 4:
            curRead.qual = line.strip()

            curStart = 0
            curEnd = splitSize
            splitCount = 0

            while curEnd < len(curRead.seq):
                
                splitID=str(writtenREAD_ID) + "_" + str(splitCount)
                #splitID=str(rid) + "_" + str(splitCount)
                myWriter.write("@" + splitID + "\n")
                myWriter.write(str(curRead.seq[curStart:curEnd]) + "\n")
                myWriter.write(str(curRead.opt) + "\n")
                myWriter.write(str(curRead.qual[curStart:curEnd]) + "\n")

                splitCount += 1
                curStart += splitSize
                curEnd += splitSize

            splitID=str(writtenREAD_ID) + "_" + str(splitCount)
            #splitID=str(rid) + "_" + str(splitCount)
            myWriter.write("@" + splitID + "\n")
            myWriter.write(str(curRead.seq[curStart:]) + "\n")
            myWriter.write(str(curRead.opt) + "\n")
            myWriter.write(str(curRead.qual[curStart:]) + "\n")

            globalVars.readHash[str(writtenREAD_ID)]=curRead.seq           
            #globalVars.readHash[str(rid)]=curRead.seq  
            
            writtenREAD_ID += 1
            seqCount += 1
            lineCount = 0

    myWriter.close()
    fqParser.close()

#function to split reads in fasta format
def splitReadsFA(inFA, oFA, splitSize):

    if inFA.endswith(".gz"):
        import gzip
        faParser = gzip.open(inFA)
    else:
        faParser = open(inFA)

    myWriter = open(oFA, "w")

    curID=""
    seqCount = 0
    writtenREAD_ID = 1
    for line in faParser:

        if line.startswith(">"):

            if curID!="":

                curStart = 0
                curEnd = splitSize
                splitCount = 0

                while curEnd < len(curRead.seq):

                    splitID=str(writtenREAD_ID) + "_" + str(splitCount)
                    curSeq=str(curRead.seq[curStart:curEnd])
                    myWriter.write(">" + splitID + "\n")
                    myIndex=0
                    # fasta format capped at 80bps per line
                    while (myIndex + 80) <= len(curSeq):
                        myWriter.write("%s\n" % (curSeq[myIndex:myIndex + 80]))
                        myIndex += 80
                    myWriter.write("%s\n" % (curSeq[myIndex:]))

                    splitCount += 1
                    curStart += splitSize
                    curEnd += splitSize

                globalVars.readHash[str(writtenREAD_ID)]=curRead.seq    

                writtenREAD_ID += 1
                seqCount += 1

            curID=line.strip()
            curRead = myFA(curID, "")

        else:
            curRead.seq+=line.strip()
        
  
    myWriter.close()
    faParser.close()
