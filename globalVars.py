import sqlite3, subprocess
###############################
#######global variables########
###############################

###################################
###classes#########################
###################################


class coGRAPH:

    def __init__(self):
        self.cGraph={}

    def addContigEdge(self, cID, sLen):
        cEdge1 = myEdge(cID + "_0", cID + "_1", sLen, sLen, 0, 'c')
        cEdge2 = myEdge(cID + "_1", cID + "_0", sLen, sLen, 0, 'c')
        self.cGraph[cID + "_0"] = myCont(cEdge1, "", {})
        self.cGraph[cID + "_1"] = myCont(cEdge2, "", {})

    def removeNode(self, cID):
        del self.cGraph[cID]
        print("%s had been removed"%(cID))

    #function to return distance between node a and b if there is a path
    def returnDist(self, a, b, circle=False):
        visited=[]
        restart=False
        #print("getting distance")
        #print(a,b)
        #print(self.cGraph[a].active)
        #print(self.cGraph[b].active)
        #print(self.cGraph[a].cEdge)
        #print(self.cGraph[b].cEdge)
        if a in self.cGraph and b in self.cGraph:
            #print("both in graph")
            nextN=a
            #print(nextN)
            #print(self.cGraph[nextN].active)
            dist=0
            while self.cGraph[nextN].active != "":
                visited.append(nextN)
                dist+=self.cGraph[nextN].active.dist
                nextN=self.cGraph[nextN].active.target
                #print(nextN, b)
                if nextN==b:
                    #print("FOUND IT")
                    if dist==0:
                        dist=1
                    return(dist)
                nextN=self.cGraph[nextN].cEdge.target
                #print(nextN)
                if nextN in visited:
                    restart=True
                    break
                visited.append(nextN)
                if circle==True:
                    if nextN==b:
                        #print("FOUND IT")
                        if dist==0:
                            dist=1
                        return(dist)
            
            if restart==True:
                #print("CIRCLE DETECTED RESTART")
                return self.returnDist(a, b, True)

            return False

        else:
            print("wut not both in graph???")
            return False


    # function to find start of a path starting with a node in the path
    def findStart(self, anode):
        inPath=[]
        #print("Finding start for node %s"%(anode))
        # just to make sure to alayws start at 0 end of contig and then go to the
        # left from there
        if anode.endswith("0"):
            cN = anode
        elif anode.endswith("1"):
            cN = self.cGraph[anode].cEdge.target
        else:
            print("ERROR incorrect scaffold name found %s" %(anode))

        inPath.append(cN[:-2])

        while self.cGraph[cN].active != "":
            cN = self.cGraph[cN].active.target
            #print(cN)
            if cN[:-2] in inPath:
                print("CIRCLE FOUND!!!!")
                #print(self.cGraph[cN].cEdge)
                #print(self.cGraph[cN].active)
                return cN
            inPath.append(cN[:-2])
            # move to other end of the contig for possible other read edge -->
            # otherwse endless loop
            # print(cN)
            cN = self.cGraph[cN].cEdge.target
        #print("Start of path found: %s"%(cN))
        return cN

    def getPath(self, anode):
        inPath=[]
        startNode=self.findStart(anode)
        curNode=startNode
        while 1:
            if curNode in inPath:
                print("getPath - Circle found!!!")
                exit()
                
            inPath.append(curNode)
            curNode=self.cGraph[curNode].cEdge.target
            inPath.append(curNode)

            if self.cGraph[curNode].active == "":
                print("End of path reached")
                return inPath
            else:
                curNode=self.cGraph[curNode].active.target
        



 # returns an alternating path of edges
    def returnEgdePath(self, anode):
        inPath=[]
        ePath=[]
        pHash={}
        curDist=0
        startNode = self.findStart(anode)
        curNode=startNode
        inPath.append(startNode[:-2])
        pHash[startNode]=(curDist, 'a')
        ePath.append(self.cGraph[curNode].cEdge)
        curDist+=self.cGraph[curNode].cEdge.dist
        curNode=self.cGraph[curNode].cEdge.target
        
        while self.cGraph[curNode].active != "":
            pHash[curNode]=(curDist, 'b')
            ePath.append(self.cGraph[curNode].active)
            curDist+=self.cGraph[curNode].active.dist
            curNode=self.cGraph[curNode].active.target
            pHash[curNode]=(curDist, 'a')
            ePath.append(self.cGraph[curNode].cEdge)
            curDist+=self.cGraph[curNode].cEdge.dist
            curNode=self.cGraph[curNode].cEdge.target
            
            if not curNode[:-2] in inPath:
                inPath.append(curNode[:-2])
        
        return ePath, pHash
    


class myCont:

    def __init__(self, cEdge, active, eList):
        self.cEdge=cEdge
        self.active=active
        self.eList=eList


# class for an edge
class myEdge:

    def __init__(self, start, target, weight, dist, stdev, direct):
        self.start = start
        self.target = target
        self.weight = weight
        self.dist = dist
        self.stdev = stdev
        self.direct = direct

    # comparing edes will comapre their weights
    def __lt__(self, other):   
        return self.weight < other.weight

    def __gt__(self, other):
        return self.weight > other.weight

    # printing function
    def __str__(self):
        return("\t%s, %s, weight:%i, dist:%i, stdev:%f, %s" % 
            (self.start, self.target, self.weight, self.dist, self.stdev, self.direct))


# class for outputting scaffolds in the end
class mySCAF:

    def __init__(self, names, seqs, dirs):
        self.names = names
        self.seqs = seqs
        self.dirs = dirs


# function to initialize global variables
def init(out, sL, mL, mO, mI, t, mSV):
    global largestContig, distError, outDir, splitLen, minOverlap, minLinks
    global minIdent, threads, contsMerged, contLenDICT, contDICT, mergedDICT, readHash
    global contigGR, adjustDICT, gapDICT, eDICT, edgeDICT
    global blastCounter, poserrorHASH, minSVL

    global insE, delE, invE, mjE, goodE
    insE={}
    delE={}
    invE={}
    mjE={}
    goodE={}

    blastCounter=0
    poserrorHASH={}
    readHash={}
    
    outDir = out
    splitLen = sL
    #variable for largest contig
    largestContig=("", 0)
    #largestMapped=[]
    #distance error is margin of read mappings
    distError=float(splitLen)*0.15*2
    minLinks = mL
    minOverlap = mO
    minIdent = mI
    threads = t
    contsMerged=0
    minSVL=mSV

    contLenDICT = {}
    contDICT = {}
    # saves the ids of merged contigs
    mergedDICT = {}
    # contig GRAPH
    contigGR = {}
    # adjust hash stores edges and saves the new edge to which they should be
    # redirected and the new distance that needs to be added to it
    adjustDICT = {}
    #for errors --> key scafID --> returns reads that need to be mapped back
    eDICT = {}
    # store readIDs of read spanning gaps
    gapDICT={}
    # for contig edges implied by hits
    edgeDICT={}

