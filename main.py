#!/usr/bin/env python3

import argparse, subprocess, sys
import statistics
import datetime
import copy
# import own scripts for stuff
import checkHitlist
from gpma import gpma
import globalVars
import splitReads
import merging

try:
    from subprocess import DEVNULL  # py3k
except ImportError:
    import os  # for python2
    DEVNULL = open(os.devnull, 'wb')
# biopython for now --> for reverse complement function
#from Bio.Seq import Seq


###############################
#######global variables########
###############################



# key is readID(without split) --> save orientation for each read to check
# for inversions later
oriHASH = {}

###################################
#############classes###############
###################################


# class for for each hit within the PAF file
class myHIT:

    def __init__(self, query, split, strand, target, t_len, t_start, t_end, q_len, q_start, q_end, mq):
        self.query = query
        self.split = split
        self.strand = strand
        self.target = target
        self.t_len = t_len
        self.t_start = t_start
        self.t_end = t_end
        self.q_len = q_len
        self.q_start = q_start
        self.q_end = q_end
        self.mq = mq

    def __str__(self):
        return("\t%s, %i, %s, %s, %s, %s, %s, %s, %s, %s, %i" % (self.query, self.split, self.strand, self.target, str(self.t_len), str(self.t_start), str(self.t_end), str(self.q_len), str(self.q_start), str(self.q_end), self.mq))
    # comparing edges will compare their weights

    def __lt__(self, other):
        return self.q_start < other.q_start

    def __gt__(self, other):
        return self.q_start > other.q_start

# class for outputting scaffolds in the end
class mySCAF:

    def __init__(self, names, seqs):
        self.names = names
        self.seqs = seqs



###################################
###functions#######################
###################################

 
#function to reverse complement a sequence
def reverseComplement(seq):
    old = "ACGT"
    replace = "TGCA"
    return seq.upper().translate(str.maketrans(old, replace))[::-1]

# function to run minimap2
def runMinimap2(nThreads, mapMode, inFA, inFQ, outPAF):
    scriptPath=sys.argv[0].rstrip("main.py")
    try:
        subprocess.call("%s/minimap2/minimap2 -t %i -x %s %s %s> %s" %
                        (scriptPath, nThreads, mapMode, inFA, inFQ, outPAF), shell=True, stdout=DEVNULL)
    except:
        print("Problem running minimap2 - check if its properly set up")

# function to parse contigs and initialize the contigGraph
def parseContigs(path, cograph):
    if path.endswith(".gz"):
        import gzip
        faParser = gzip.open(path, mode='rt')
    else:
        faParser = open(path)
    # for first iteration/line
    seq = ""
    for line in faParser:
        if line.startswith(">"):
            if seq != "":
                seqLen = len(seq)
                # initalize contigGraph
                cograph.addContigEdge(contID, seqLen)               
                # initalize contLenDICT
                globalVars.contLenDICT[contID] = seqLen
                globalVars.contDICT[contID] = seq
                globalVars.insE[contID]=[] # insertions 
                globalVars.invE[contID]=[] # inversions
                globalVars.delE[contID]=[] # deletions
                globalVars.mjE[contID]=[] # misjoins
                globalVars.goodE[contID]=[] # continuous mapping hits
                if seqLen>=globalVars.largestContig[1]:
                    globalVars.largestContig=(contID, seqLen)

            contID = line.strip().split(" ")[0][1:]
            seq = ""
        else:
            seq += line.strip()

    faParser.close()

    # for last sequence not covered by loop
    seqLen = len(seq)
    # initalize contigGraph
    cograph.addContigEdge(contID, seqLen)
    # initalize globalVars.contLenDICT
    globalVars.contLenDICT[contID] = seqLen
    globalVars.contDICT[contID] = seq
    globalVars.insE[contID]=[]
    globalVars.invE[contID]=[]
    globalVars.delE[contID]=[]
    globalVars.mjE[contID]=[]
    globalVars.goodE[contID]=[]



#function to bundle a set of edges between two nodes
#as described in gpma
def bundleEdges(edgeSet, noMed=False):
    #print("BUNDELING:")
    #print(edgeSet)
    nm=False
    bundledList=[]
    if noMed ==True: # if median picked before clusered nothing take first edge
        #print("Taking median failed before so now just pick first edge")
        medianEdgeLen=edgeSet[0]
    else:
        medianEdgeLen = statistics.median(edgeSet) #calc median
    #stDev=numpy.std(edgeSet)
    #print("Median edge chosen: %i"%(medianEdgeLen))
    #print(stDev, globalVars.distError*2)
    newWeight=0
    fitDists=[]
    #if edgeSet only has length one to avaoid unnecessary computations
    if len(edgeSet) == 1 and globalVars.minLinks<=1:
        bundledList=[(1, edgeSet[0], 0)]
        edgeSet.remove(edgeSet[0])

    else:
        for dist in edgeSet:
            #print(abs(medianEdgeLen - dist), 2*globalVars.distError)
            if abs(medianEdgeLen - dist) < 2*globalVars.distError:
                newWeight+=1
                fitDists.append(dist)
                #print("dist %i fit and was added and removed!"%dist)
        # important to remove after loop otherwise it causes the error of skipping some edges
        for dist in fitDists:
            edgeSet.remove(dist)
        if len(fitDists) > 1:
            # bundle edges as defined in GPMA
            p=sum([dist/((globalVars.distError*2)**2) for dist in fitDists])
            q=sum([1/((globalVars.distError*2)**2) for dist in fitDists])
            newDist=int(p/q)
            newSDev=statistics.stdev(fitDists)
            newEdge=(newWeight, newDist, newSDev)
            #print("bundeled new edge added:")
            #print(newEdge)
            #print(fitDists)
            bundledList.append(newEdge)
        elif len(fitDists) == 0:
            #print("Bundeling failed")
            nm=True
            #if globalVars.minLinks<=1:               
            #    bundledList+=[(1, edge, 0) for edge in edgeSet]
            #    edgeSet=[]
            
        elif len(fitDists) == 1 and globalVars.minLinks<=1:
            newDist=fitDists[0]
            bundledList.append((1, fitDists[0], 0))
            
    
    #newEdge = (edgeWeight, medianEdgeLen)
    

    # repeat if still unbundled edges left 
    if len(edgeSet)>0:
        #print("recall bundeling there is still some edges left")
        bundledList+=bundleEdges(edgeSet, nm)
    #print("\n\n")
    return(bundledList)


# parses PAF gets hitlists and checks them --> builds edges and bundles them
# returns set of bundled edges
def parsePAF(path):
    if path.endswith(".gz"):
        import gzip
        pafParser = gzip.open(path)
    else:
        pafParser = open(path)

    # list for all hit of one reads --> e.g. best hit per split part
    hitList = []
    posCounts = 0
    revCounts = 0

    for line in pafParser:
        splitted = line.strip().split("\t")
        query = splitted[0]
        q_len = int(splitted[1])
        q_start = int(splitted[2])
        q_end = int(splitted[3])-1
        strand = splitted[4]
        tar = splitted[5]
        t_len = int(splitted[6])
        t_start = int(splitted[7])
        t_end = int(splitted[8])-1
        #matching = splitted[9]
        #opts = splitted[10]
        mq = int(splitted[11])
        #P for primary aln or S for secondary
        #alnType=splitted[12].split(":")[-1]

        # get fragment ID
        qID = query.split("_")[0]
        splitID = int(query.split("_")[-1])

        curHIT = myHIT(qID, splitID, strand, tar, t_len,
                       t_start, t_end, q_len, q_start, q_end, mq)

        # take only the best hit for each fragment --> replace if the last one was

        if q_end-q_start>q_len*0.3:
            if len(hitList)==0:
                hitList.append([curHIT])
                continue
            if curHIT.query == hitList[-1][0].query:
                #if curHIT.split == lastHit.split:
                
                if curHIT.split == hitList[-1][0].split:
                    #print("Mult hit found")
                    #print(lastHit)
                    #print(curHIT)
                    #identical hit
                    hitList[-1].append(curHIT)           
                else:
                    hitList.append([curHIT])
                    if curHIT.strand == "+":
                        posCounts += 1
                    elif curHIT.strand == "-":
                        revCounts += 1

            else:
                # more than 1 hit --> only then it makes sense to check the
                # hitlist
                if len(hitList) > 1:
                    #check the hitlist to identify SVs misjoins and edges
                    checkHitlist.checkHitlist(hitList, globalVars.edgeDICT, oriHASH)

                # start hit list for new read and reset repCounter
                hitList = []
                if q_end-q_start>=q_len*0.3:
                    hitList.append([curHIT])
                    if curHIT.strand == "+":
                        posCounts += 1
                    elif curHIT.strand == "-":
                        revCounts += 1

    #for last hitlist
    checkHitlist.checkHitlist(hitList, globalVars.edgeDICT, oriHASH)

    pafParser.close()

    
    print("\t2.1) Bundling Edges")
    edgeLIST = []
    # bundleEdges
    for cont in globalVars.edgeDICT:
        for tCont in globalVars.edgeDICT[cont]:
            bundeledEdges=bundleEdges(globalVars.edgeDICT[cont][tCont])
            #print(bundeledEdges)
            globalVars.edgeDICT[cont][tCont] = bundeledEdges

            # ignore edges with only weight 1
            acceptedEdges=[]
            for edge in globalVars.edgeDICT[cont][tCont]:
                
                #print(edge)
                if edge[0] >= globalVars.minLinks:
                    
                    #check if overlap so large it indicates a merge
                    if edge[1]*-1 >= globalVars.contLenDICT[cont[:-2]] or edge[1]*-1 >= globalVars.contLenDICT[tCont[:-2]]:
                        #print("overlap so large it indicates a merge")
                        if not edge[1]*-1 > 50000:
                            merged, realOL, mAdjust = merging.runBLAST(cont, tCont, edge[1], True)
                            if merged=="Removed":
                                print("Merging passed")
                                #removeMergedNEW(remove[0][:-2]+"_0", remove[1], remove[2])
                                #removeMergedNEW(remove[0][:-2]+"_1", remove[3], remove[4])
                            #else:
                            #    print("Merging failed")
                    else:
                    
                        newEdge = globalVars.myEdge(cont, tCont, edge[0], edge[1], edge[2], 'r')
                        acceptedEdges.append(edge)
                        ins = False
                        for i, e in enumerate(edgeLIST):
                            if e < newEdge:
                                edgeLIST.insert(i, newEdge)
                                ins = True
                                break

                        if ins == False:
                            edgeLIST.append(newEdge)

                        #print("Accepted edges between %s and %s"%(cont, tCont))
                        #for e in acceptedEdges:
                        #    print(e)
                        #print("\n")
            globalVars.edgeDICT[cont][tCont]=acceptedEdges

    return edgeLIST


# running GPMA from gpma.py
def runGPMA(orderedEdges, cGRAPH):
    writer=open(globalVars.outDir + "/tmp_edges", "w")
    for i, edge in enumerate(orderedEdges):
        #print("Running GPMA trying to set edge (%i/%i):"%(i, len(orderedEdges)-1))
        #print(edge) 
        gpma(edge, cGRAPH)
        writer.write(str(edge)+"\n")
    writer.close()

#function to cluster variants per contig
def clusterSVs(svList, cMap, writer, ms, misJoin=False, deletion=False):
    #to avoid unnecessary checking
    if len(svList) < 2:
        return []

    breaks=[]
    import networkx as nx
    G = nx.Graph()
    nINS=len(svList)
    #print(svList)     
    for i, sv1 in enumerate(svList):
        for j in range(i+1, nINS):
            sv2 = svList[j]
            #print(sv1, sv2)
            sd = abs((sv1[2]-sv1[1]) - (sv2[2]-sv2[1])) / max((sv1[2]-sv1[1]), (sv2[2]-sv2[1]), 1)
            pd = min(abs(sv1[1]-sv2[1]), abs(sv1[2]-sv2[2]), abs((sv1[2]+sv1[1])/2 - (sv2[2]+sv2[1])/2))
            #print(sd, pd)
            spd=sd+pd/2000
            #print(spd)
            if spd < 0.7:
                #print("CLUSTER TOGETHER")
                G.add_edge(i, j)                   
    #print("\n")
    insClusts=nx.find_cliques(G)
    #print("nClusters found:")
    for iClust in insClusts:
        #print(len(iClust))
        #print(iClust)
        if len(iClust) > 2:
            svB=[]
            svE=[]
            svW=0
            scaf=""
            for i in iClust:
                #print(svList[i])
                ins = svList[i]
                scaf=ins[0]
                svB.append(ins[1])
                svE.append(ins[2])
                svW+=1
                #print(ins)
            
            
            #print(svB, svE)

            if misJoin==True:
                svB=int(statistics.mean(svB))
                svE=int(statistics.mean(svE)+100)
                if svE >= len(cMap):
                    if svB >= len(cMap):
                        continue
                    svE=len(cMap)-1
                #print(svB, svE)
                meanGood=round(statistics.mean(cMap[svB:svE+1]))
                meanSTD=round(statistics.stdev(cMap[svB:svE+1]))
                meanGood+=(-3)*meanSTD
            else:
                svB=int(statistics.mean(svB))
                svE=int(statistics.mean(svE)) 
             
            meanGood=round(statistics.mean(cMap[svB:svE+1]))
            if deletion==True:
                svW+=meanGood
            #print(svB, svE, svW, meanGood, svW/(svW+meanGood))  
            if svW > 2 and svW/(svW+meanGood)>=0.7: 
                #print("ACCEPT SV!!!")
                #print(cMap[svB:svE+1])
                #print(svB, svE, svW, meanGood, svW/(svW+meanGood))
                #breaks.append((svB, svE))
                breaks.append(svB)
                breaks.append(svE)
                writer.write("%s\t%i\t%i\t%i\t%i\t%f\n"%(scaf ,svB, svE, svW, meanGood, svW/(svW+meanGood)))
            cMap[svB]-=svW
            if svB!=svE:
                cMap[svE]-=svW

            
    G.clear()

    return breaks

# function to break contigs given a 
def breakConts(bHASH, cit):
    oWriter=open(args.outdir+"/"+"adjusted_contigs_it%i.fa"%(cit), "w")
    for scaf in globalVars.contDICT:
        if scaf in bHASH:
            curStart=0
            counter=1
            breakEMargin=500
            for error in bHASH[scaf]:
                #print(curStart, error, error-breakEMargin)
                #only output fragments larger than 1000bp
                if len(globalVars.contDICT[scaf][curStart:error-breakEMargin]) > 1000:
                    oWriter.write(">%s_%i\n"%(scaf, counter))
                    #print("Write splitted scaf: %s from: %i-%i" %(scaf, curStart, error-breakEMargin))
                    oWriter.write("%s\n"%(globalVars.contDICT[scaf][curStart:error-breakEMargin]))

                curStart=error+breakEMargin
                counter+=1
            if len(globalVars.contDICT[scaf][curStart:]) > 1000:
                oWriter.write(">%s_%i\n"%(scaf, counter))
                #print("Write splitted scaf: %s from: %i-%i" %(scaf, curStart, len(globalVars.contDICT[scaf])))
                oWriter.write("%s\n"%(globalVars.contDICT[scaf][curStart:]))                              
        else:
            oWriter.write(">%s\n"%(scaf))
            oWriter.write("%s\n"%(globalVars.contDICT[scaf]))                       
    oWriter.close()

# function that generates the pos. coverage map per contig
# and then runs clusterSVs on the different SVs for that contig
# and then checks if any contigs must be broken (breakConts)
def checkContigsNEW(curIt, noBreak):
    svbreak=False
    covWriter=open(globalVars.outDir + "/coverage_map_it%i.txt"%(curIt), "w")
    insWriter=open(globalVars.outDir + "/insertions_it%i.txt"%(curIt), "w")
    invWriter=open(globalVars.outDir + "/inversions_it%i.txt"%(curIt), "w")
    delWriter=open(globalVars.outDir + "/deletions_it%i.txt"%(curIt), "w")
    mjWriter=open(globalVars.outDir + "/misjoins_it%i.txt"%(curIt), "w")
    svBreaks={}
    for scaf in globalVars.goodE:
        #print("Checking scaf %s"%(scaf))
        covMap=[0]*globalVars.contLenDICT[scaf]
        breakpoints=[]
        for g in globalVars.goodE[scaf]:
            for i in range(g[1],g[2]+1):
                covMap[i]+=1
        meanSupport=statistics.median(covMap[50:-50])
        #print("cont %s has a mean goodness of scaf %f"%(scaf, meanSupport))
        #print(covMap)
        #print("INSERTIONS:")
        breakpoints+=clusterSVs(globalVars.insE[scaf], covMap, insWriter, meanSupport)
        #print("DELETIONS:")
        breakpoints+=clusterSVs(globalVars.delE[scaf], covMap, delWriter, meanSupport, deletion=True)
        #print("INVERSIONS:")
        breakpoints+=clusterSVs(globalVars.invE[scaf], covMap, invWriter, meanSupport)
        #print("MISJOINS:")
        breakpoints+=clusterSVs(globalVars.mjE[scaf], covMap, mjWriter, meanSupport, misJoin=True)

        if breakpoints!=[]:
            svbreak=True
            svBreaks[scaf]=breakpoints

        checkCovMap(scaf, covMap, covWriter)


    if svbreak==True and noBreak==False:
        breakConts(svBreaks, curIt)

    covWriter.close()
    insWriter.close()
    delWriter.close()
    invWriter.close()
    mjWriter.close()

    return svbreak

# outputs the coverage map
def checkCovMap(curCont, cmap, oWriter):
    oWriter.write(">%s\n"%(curCont))
    totalBps=0
    nGood=0
    nBad=0
    nUnknown=0
    curRange=["", "", 0]

    for pos, cov in enumerate(cmap):
        totalBps+=1
        if cov>0:
            nGood+=1
        elif cov==0:
            nUnknown+=1
        else:
            nBad+=1
        if pos==0:
            curRange[0]=pos
            curRange[1]=pos
            curRange[2]=cov
        else:
            if curRange[2]==cov:
                curRange[1]=pos
            else:
                oWriter.write("\t%s\t%i\t%i\t%i\n"%(curCont, curRange[0], curRange[1], curRange[2]))
                curRange[0]=pos
                curRange[1]=pos
                curRange[2]=cov
    oWriter.write("\t%s\t%i\t%i\t%i\n"%(curCont, curRange[0], curRange[1], curRange[2]))                     
                

# function to write a scaffold for final output
def writeScaf(sSeqs, sNames, oNR, sWriter, gffWriter):
    #Write output
    newSeq = ""
    #print("writing out")
    
    
    for i, name in enumerate(sNames):                        
        seq = sSeqs[i]
        start=len(newSeq)+1
        newSeq += seq
        end=len(newSeq)
        if name.startswith("read_"):
            gffWriter.write("scaf_%i\t.\tread(%s)\t%i\t%i\n"%(oNR, name, start, end))
        else:
            gffWriter.write("scaf_%i\t.\tcontig(%s)\t%i\t%i\n"%(oNR, name, start, end))
    
            
    scafLen = len(newSeq)
    sWriter.write(">scaf_%i(len=%i)\n" % (oNR, scafLen))
    # print(newName)
    # print(newSeq)
    myIndex = 0
    # fasta format capped at 80bps per line
    while (myIndex + 80) <= len(newSeq):
        sWriter.write("%s\n" % (newSeq[myIndex:myIndex + 80]))
        myIndex += 80
    sWriter.write("%s\n" % (newSeq[myIndex:]))

    return scafLen

   

# function to output all contigs/scaffolds in the end --> gets the path
# for each and then puts them together before writing out
def getPATH(myGraph):
    scafWriter = open(globalVars.outDir + "/scaffolds.fasta", "w")
    scafCount = 0
    idWriter = open(globalVars.outDir + "/conts_scafs.ids", "w")
    gffWriter = open(globalVars.outDir + "/scaffolds.gff", "w")
    # hash to keep track of already visited contigs
    cLens=[]
    visited = {}
    count=1
    #print(len(myGraph.cGraph))
    for cont in myGraph.cGraph:
        #print("Checking cont %s - %i/%i"%(cont, count, len(myGraph.cGraph)))
        count+=1
        if not cont[:-2] in visited:
            startNode = myGraph.findStart(copy.copy(cont))
            #print(myGraph.getPath(copy.copy(cont)))
            first=True
            #print("Getting path for cont %s start of path is at: %s"%(cont, startNode))

            newScaf = mySCAF([], []) # intitalize output scaf
            idWriter.write(">scaf_%i\n"%(scafCount))
            curNode=copy.copy(startNode)
            while 1:
                #print("while curNode=%s"%(curNode))
                # check if already visited
                if not curNode[:-2] in visited:
                    visited[curNode[:-2]]=1
                else:
                    #print("CIRCLE FOUND")
                    curLen=writeScaf(newScaf.seqs, newScaf.names, scafCount, scafWriter, gffWriter)
                    cLens.append(curLen)
                    scafCount+=1
                    idWriter.write("\n")
                    break 
                visited[curNode[:-2]] = 1
                #print("visited %s"%(curNode[:-2]))

                #case for startNode set dist to 0
                if first==True:
                    first=False
                    offset=0                  
                else:
                    offset = myGraph.cGraph[curNode].active.dist *(-1) # change pos/neg to get offset for contigSeq
                if myGraph.cGraph[curNode].active != "":
                    idWriter.write("\tdist=%i, links=%i\n"%(offset*-1, myGraph.cGraph[curNode].active.weight))
                else:
                    idWriter.write("\tdist=%i\n"%(offset*-1))


                
                # get scaf seq in correctOrientation
                if curNode[-1]=="0":
                    contSeq = globalVars.contDICT[curNode[:-2]]
                elif curNode[-1]=="1": # reverse complement seq
                    #contSeq=str(Seq(globalVars.contDICT[curNode[:-2]]).reverse_complement())
                    contSeq=reverseComplement(globalVars.contDICT[curNode[:-2]])

                
                idWriter.write("\t%s(len=%i)\n"%(curNode, globalVars.contLenDICT[curNode[:-2]]))
                
                #fill gap
                if offset < 0: # gap that must be filled exists
                    #print("Filling gap between %s and %s"%(myGraph.cGraph[curNode].active.target, curNode))
                    fillID, fillSeq = fillEDGEgap(myGraph.cGraph[curNode].active.target, curNode, newScaf.seqs[len(newScaf.seqs)-1], contSeq, offset*-1)
                    #print("Filler = %s len=%i"%(fillID, len(fillSeq)))

                    if fillID=="100N":
                        #print("Dont set edge")
                        del visited[curNode[:-2]]
                        myGraph.cGraph[curNode].active=""
                        curLen=writeScaf(newScaf.seqs, newScaf.names, scafCount, scafWriter, gffWriter)
                        cLens.append(curLen)
                        scafCount+=1
                        idWriter.write("\n")
                        break                       
                    else:    
                        newScaf.names.append("read_"+fillID)
                        newScaf.seqs.append(fillSeq)

                        offset = 0

                #print("appending cont %s with offset %i"%(curNode, offset))
                newScaf.names.append(curNode)
                #print("appending seq of len=%i"%(len(contSeq[offset:])))
                newScaf.seqs.append(contSeq[offset:])
                
                curNode = myGraph.cGraph[curNode].cEdge.target
                
                if myGraph.cGraph[curNode].active=="":
                    #print("End of path reached")
                    curLen=writeScaf(newScaf.seqs, newScaf.names, scafCount, scafWriter, gffWriter)
                    cLens.append(curLen)
                    scafCount+=1
                    idWriter.write("\n")
                    break
                else:
                    #print(myGraph.cGraph[curNode].active)
                    curNode = myGraph.cGraph[curNode].active.target
                    #print("Next in path is %s"%(curNode))
                    #print(myGraph.cGraph[curNode].active)
    
    scafWriter.close()
    idWriter.close()
    gffWriter.close()
    return cLens


# function to fill the gap between to contigs with a pacbio read
def fillEDGEgap(cont1, cont2, cSeq1, cSeq2,  gLen):
    gapFilled=False
    #print("Trying to fill gap between %s and %s "%(cont1, cont2))
    #sort contigs to get same order as before for edge connection
    if cont1 < cont2:
        node1 = cont1
        node2 = cont2
    else:
        node1 = cont2
        node2 = cont1
    #print(node1, node2)
    gapReads=globalVars.gapDICT[node1][node2]

    if len(cSeq1)>1000:
        adjustedSeq1=cSeq1[-1000:]
        adjustedLen1=1000
    else:
        adjustedSeq1=cSeq1
        adjustedLen1=len(cSeq1)
    if len(cSeq2)>1000:
        adjustedSeq2=cSeq2[:1000]
        adjustedLen2=1000
    else:
        adjustedSeq2=cSeq2
        adjustedLen2=len(cSeq2)

    for read in gapReads:
        #print("\tUsing read %s" %(read))        
        rSeq=globalVars.readHash[read]
        gapFilled=merging.fillGAPS(cont1, cont2, adjustedLen1, adjustedLen2, adjustedSeq1, adjustedSeq2, read, len(rSeq), rSeq)

        #if gap can be filled abort gapFilling process
        if gapFilled!=False:
            break
    
    if gapFilled==False:
        read="100N"
        gapFilled="N"*100
        #print("GAP could not be filled --> add 100 Ns in between???!!!")
        #print(cont1, cont2)

    return read, gapFilled

# calculates the N-statistics (N10,..,N50,..,N100)
def calcNstats(scafLens, totalLen):
    Nvals=[(0,0)]*10
    n=0
    curSum=0
    curN=0.1
    curPos=0
    for slen in scafLens:
        curSum+=slen
        #print(curN, curPos, curSum, n, slen)
        n+=1
        curThresh=totalLen*curN
        #print("cur threshold %f"%curThresh)
        if curSum >= curThresh:
            curN=round(curN+0.1, 2)
            Nvals[curPos]=(slen, n)
            curPos+=1

    return(Nvals)               

# function to generate and ouput the scaffolds statistics
def scafStats(sLens):
    writer=open(globalVars.outDir + "/scaffolds.stats", "w")
    sortedLens=sorted(sLens, reverse=True)
    totalBps=sum(sLens)
    totalScafs=len(sLens)
    avgLen=statistics.mean(sLens)
    sortedLens=sorted(sLens, reverse=True)

    writer.write("Total number basepairs in assembly: %i\n"%(totalBps))
    writer.write("Total number of scaffolds: %i\n"%(totalScafs))
    writer.write("Longest scaffold: %i\n"%(sortedLens[0]))
    writer.write("Average scaffold length: %i\n"%(avgLen))

    nVs=calcNstats(sortedLens, totalBps)

    names=["N10","N20","N30","N40","N50","N60","N70","N80","N90","N100"]
    for i, nv in enumerate(nVs):
        writer.write("%s = %i, n = %i\n"%(names[i], nv[0], nv[1]))

    writer.close()

# function to remove all tmp files created while running the script
def removeTMPfiles():
    subprocess.call("rm %s/tmp*" % (globalVars.outDir), shell=True)



###########################################################################
###########################RUN EVERYTHING##################################
def run(genome, nobreak, curIter):
    # create output directory if it not exists already
    subprocess.call("mkdir -p %s" % (args.outdir), shell=True)

    # initlaiye global variables
    if curIter > 0:
        tempStore=globalVars.readHash
    globalVars.init(args.outdir, args.size, args.minlinks, args.minoverlap, args.minident, args.threads, args.minSVLen)
    
    if curIter>0:
        globalVars.readHash=tempStore
        tempStore=""
    contigGraph=globalVars.coGRAPH()

    print("0a) Parsing Contigs")
    parseContigs(genome, contigGraph)


    t0=datetime.datetime.now()
    splitFile = args.outdir + "/tmp_%ibp_splits" % (args.size)
    if curIter == 0:
        print("\n0b) Splitting reads into %ibp chunks" % (args.size))
        if args.fasta:
            splitReads.splitReadsFA(args.fastq, splitFile, args.size) 
        else:            
            splitReads.splitReadsFQ(args.fastq, splitFile, args.size)
    t1=datetime.datetime.now()
    splittime=t1-t0

    print("\n1) Running minimap2")
    # name for output file
    t0=datetime.datetime.now()
    myPAF = globalVars.outDir + "/tmp_it%i.paf"%(curIter)
    runMinimap2(args.threads, mapMode, genome, splitFile, myPAF)
    t1 = datetime.datetime.now()
    maptime=t1-t0

    print("\n2) Parsing PAF")
    t0=datetime.datetime.now()
    edgeSet = parsePAF(myPAF)
    t1=datetime.datetime.now()
    parseTime=t1-t0

    #avgCOV=statistics.median(globalVars.largestMapped)
    #print(globalVars.largestMapped)
    #print("\tCalculated average coverage: %i"%(avgCOV))

    print("\n3) Clustering and identification of SVs and misjoins")
    t0 = datetime.datetime.now()
    restart=checkContigsNEW(curIter, nobreak)
    t1 = datetime.datetime.now()
    svTime=t1-t0
    #restart, contStats = checkContigs(curIter, nobreak)
    if restart==True and nobreak==False:
        print("\tContigs were adjusted restart needed!!!")
        return False
    else:

        # check if contigs need to be readjusted if so --> readjust and rerun
        # mapping before running GPMA
        print("\n4) Running GPMA")
        print("\tNumber of edges: %i" %(len(edgeSet)))
        t0 = datetime.datetime.now()
        runGPMA(edgeSet, contigGraph)
        t1 = datetime.datetime.now()
        gpmaTime=t1-t0

        # get the paths between contigs for output
        print("\n5) Outputting scaffolds and filling gaps")
        t0 = datetime.datetime.now()
        scafLengths=getPATH(contigGraph)
        t1 = datetime.datetime.now()
        getPathTime=t1-t0

        haploWriter = open(globalVars.outDir + "/duplicates.fasta", "w")
        print("\n6) Outputting unscaffolded duplicates")
        #output haplotigs
        visited={}
        for ele in globalVars.adjustDICT:
            sID=ele[:-2]
            if visited.get(sID, "nope")==1:
                continue
                
            contSeq = globalVars.contDICT[sID]
            haploWriter.write(">%s\n%s\n"%(sID, contSeq))
            
            visited[sID]=1
        haploWriter.close()


        #get stats
        scafStats(scafLengths)

        
        # cleaning up:
        print("\n7) Cleaning up - removing tmp files")
        removeTMPfiles()


        print("\n\n")

        etime = datetime.datetime.now()
        totaltime = etime - stime
        #print(globalVars.contsMerged)
        print("Total elapsed time: " + str(totaltime) + " [h:min:sec:millisec]")
        print("0) Splitting time: " + str(splittime) + " [h:min:sec:millisec]")
        print("1) Mapping time(minimap2): " +
            str(maptime) + " [h:min:sec:millisec]")
        print("2) Parsing time: " + str(parseTime) + " [h:min:sec:millisec]")
        print("2) SV clustering time: " + str(svTime) + " [h:min:sec:millisec]")    
        print("4) GPMA time: " + str(gpmaTime) + " [h:min:sec:millisec]")
        #print("\tTotal BLASTS done: %i"%(globalVars.blastCounter))
        print("5) GetPath time: " + str(getPathTime) + " [h:min:sec:millisec]")


        return True
        


##########################################################################
##########################################################################
##########################################################################
parser = argparse.ArgumentParser(description="")
parser.add_argument(
    "-ge", "--genome", help="Contigs in fasta format", type=str, required=True)
parser.add_argument(
    "-fq", "--fastq", help="Reads in fastq format", type=str, required=True)
parser.add_argument(
    "-out", "--outdir", help="Directory for output", type=str, required=True)
parser.add_argument(
    "-m", "--mode", help="Mapping mode in minimap2 for different types of input data data. Options: pb/ont [default=pb]", type=str, default="pb")
parser.add_argument(
    "-t", "--threads", help="Threads to use for minimap2 only [default=4]", type=int, default=4)
parser.add_argument(
    "-ml", "--minlinks", help="Minimum reads supporting a link needed to set an edge between two contigs [default=2]", type=int, default=2)
parser.add_argument(
    "-mo", "--minoverlap", help="Minimum overlap needed for two contigs to be merged [default=50]", type=int, default=50)
parser.add_argument(
    "-mi", "--minident", help="Minimum identity in overlap needed for two contigs to be merged [default=85.0]", type=float, default=85.0)
parser.add_argument(
    "-it", "--iterations", help="maximal iterations performed for breaking set to 0 to perfrom NO breaking", type=int, default=2)
parser.add_argument(
    "-msvl", "--minSVLen", help="Miniumum length of a structural variant (insertion, deletion, inversion) to break a contig for [default=1000]", type=int, default=1000, choices=range(500, 10001), metavar="[500, 10000]")
parser.add_argument(
    "-s", "--size", help="Size to split the input reads into (in bps) [default=1000]", type=int, default=1000)
parser.add_argument(
    "-fa", "--fasta", help="Enable if input reads are in fasta format", action="store_true")
args = parser.parse_args()


##########################################################################
if __name__ == "__main__":
    # starttime
    stime = datetime.datetime.now()
    # get right mode
    if args.mode == "pb":
        mapMode = 'map-pb'
    elif args.mode == "ont":
        mapMode = 'map-ont'


    genome=args.genome
    curIt=0
    mode=False
    while curIt <= args.iterations:
        print("---------------------------------------------ITERATION %i---------------------------------------------"%(curIt))
        if curIt == args.iterations:
            mode=True
        end=run(genome, mode, curIt)
        genome=args.outdir+"/"+"adjusted_contigs_it%i.fa"%(curIt)
        curIt+=1
        if end == True:
            curIt=args.iterations+1
        print("\n\n")