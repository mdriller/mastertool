# REAPRLong: Improvement and QC of genome assemblies using (low coverage) long reads
<p align="center"> 
<img src="figures/simple_workflow.svg">
</p>

### Dependencies/Prerequisites:
1. Python3 →  version >3.6 necessary  
2. Python library networkx →  can be installed using pip (pip install networkx)  
3. minimap2 →  provided within the REAPRLong git repository (needed at this path)  


### Setup:
REAPRLong is publically available to download and use in the git repository https://git.imp.fu-berlin.de/mdriller/mastertool.  
The minimap2 gitlab repository is added as a submodule within REAPRLong, to include it in the download please run:   
git clone --recursive https://github.com/mdriller/masterTool.git  
Then move into the minimap2 directory and build it   
cd masterTool/scripts/minimap2/   
make  


### Usage:
The main script is made executable and can be executed if python3 exists at: "#!/usr/bin/env python3".   
Otherwise the script can be run using python directly: "python main.py ..."   
The help function can be accessed via: ./main.py -h|--help and provides a general overview of how to use and which parameters can be set when using the tool.   
REAPRLong needs a genome assembly in fasta format, long reads (e.g. PacBio or ONT) in fastq or fasta format and a path, where output files will be generated, as mandatory input to run. Additional parameters can be set but the default values are tested and generally provide the best results.  
REAPRLong can be used as follows:   

<p align="center"> 
<img src="figures/helpfunction.png">
</p>

 Example Usage:
 
./main.py -s 500 -ge /home/max/tests/genome.fasta -fq /home/max/tests/pacbioReads.fastq -out /home/max/tests/output -it 3
  
### Output files:
  
REAPRLong generates multiple output files in the specified output directory.

1. **scaffolds.fasta** - the generated scaffolds in fasta format  
2. **scaffolds.stats** - statistics generated for scaffolds.fasta (total basepairs in the assembly, number of scaffolds, longest scaffold, average length and N10/20/30/40/ 50/60/70/80/90/100 values)  
3. **scaffolds.gff** - gff3 file describing the regions of each new scaffold. Regions can either come from previous contigs or from reads if a gap was filled.  
4. **duplicates.fasta** - fasta file containing contigs that were fully part of another contig and thus removed from the assembly.  
5. **adjusted\_contigs\_it\*.fa** - fasta file containing adjusted contigs, if the QC identified misassemblies and broke the previous input. The \* is an integer value indicating the iteration of QC, starting with 0.   
6. **coverage\_map\_it\*.gff** - a "coverage" map for the input assembly of each iteration. Regions are summarised giving a start and end position and the support given for the region. The support describes the amount of reads mapping continuously in the region substracted by the amount of reads mapping dis-continuously. Negative numbers indicate misassemblies.  
7. **deletions\_it\*.txt** - identified deletions (within the genome compared to the reads). The \* is an integer value indicating the iteration of QC, starting with 0 which represents the original assembly. Every subsequent number relates to the adjusted\_contigs\_it\*.fa of the previous iteration.  
8. **insertions\_it\*.txt** - identified insertions (within the genome compared to the reads). The \* is an integer value indicating the iteration of QC, starting with 0 which represents the original assembly. Every subsequent number relates to the adjusted\_contigs\_it\*.fa of the previous iteration.  
9. **inversions\_it\*.txt** - identified inversions (within the genome compared to the reads). The \* is an integer value indicating the iteration of QC, starting with 0 which represents the original assembly. Every subsequent number relates to the adjusted\_contigs\_it\*.fa of the previous iteration.  
10. **misjoins\_it\*.txt** - identified misjoins (within the genome compared to the reads). The \* is an integer value indicating the iteration of QC, starting with 0 which represents the original assembly. Every subsequent number relates to the adjusted\_contigs\_it\*.fa of the previous iteration.  
### Workflow
<p align="center"> 
<img src="figures/workflow_noOptional.svg">
</p>

