import globalVars

# function to compare to fragments on same scaf to check for insertions
# and deletions
def checkINSDELS(hit1, hit2):
    curError=None
    curCorrect=None
    #print("check INS/DELS")
    hdist = ((hit2.split - hit1.split) - 1) * hit1.q_len

    if hit1.strand == "+" and hit2.strand == "+": #both map on fwd strand
        h1_end = min(hit1.t_end + (hit1.q_len - hit1.q_end), hit1.t_len-1)
        h2_start = max(hit2.t_start - hit2.q_start, 0)
        dist = h2_start - h1_end
        diffDIST = dist - hdist
        eStart=hit1.t_end
        eEnd=hit2.t_start
        cstart=hit1.t_start
        cend=hit2.t_end

    elif hit1.strand == "-" and hit2.strand == "-": #both map on rev strand
        h1_end = min(hit1.t_start - (hit1.q_len - hit1.q_end), hit1.t_len-1)
        h2_start = max(hit2.t_end + hit2.q_start, 0)
        dist = h1_end - h2_start
        diffDIST = dist - hdist
        eEnd=hit1.t_start
        eStart=hit2.t_end-1
        cstart=hit1.t_end
        cend=hit2.t_start
    #if dist error larger(pos or neg) than min SV size
    if diffDIST >= globalVars.minSVL or diffDIST <= -1*globalVars.minSVL:  
        #print("Error detected")
        #print(hit1)
        #print(hit2)
        #print(eStart, eEnd, diffDIST)
        curError=(hit1.target, min(eStart, eEnd), max(eStart, eEnd), diffDIST)
        #if diffDIST < 0:
        #    print("DELETION!!!")
    else:
        #print("Correct mapping")
        curCorrect=(min(cstart, cend), max(cstart, cend))
        #print(curCorrect)
   # print(hit1)
   # print(hit2)
   # print(curCorrect)
   # print(curError)
    return curError, curCorrect
          

# fucntion to identify inversions e.g. compare hits in different orientations on same contig
def checkINVS(hit1, hit2, oriHASH):
    #print("check INVS")
    # to store the hits/fragments of inversion --> start and end
    #hdist = ((hit2.split - hit1.split) - 1) * hit1.q_len
    #readOri = oriHASH[hit1.query]

    curError=None

    #if hit2.strand != oriHASH[hit2.query]:
    invStart=min(hit1.t_start, hit2.t_start)
    invEnd=max(hit1.t_start, hit2.t_start)
    curError=(hit1.target, invStart, invEnd, invEnd-invStart) 
    return curError


# function to calculate the distance between to contigs
# if both splits map on rev strand
def calcHitContig_Dist_rev(hit1, hit2):
    err1=False
    err2=False
    h1_end = hit1.t_start - (hit1.q_len - hit1.q_end) # rest of contig 1 after extending aln to theoretical end can be negative
    h2_start = hit2.t_len - (hit2.t_end + hit2.q_start)# rest of contig 2 after extending aln to theoretical end can be negative
    # hdist->=distance between splits
    if hit1.split != hit2.split:
        hdist = ((hit2.split - hit1.split) - 1) * hit1.q_len
    else:
        hdist = 0
    # calc contig distance
    cdist = hdist - (h1_end + h2_start)
    if h1_end > hdist and h1_end > globalVars.splitLen:
        #print("add error %s for cont1"%(hit1.target))
        #print(h1_end, hdist)
        #print(hit1)
        err1=True
    if h2_start > hdist and h2_start > globalVars.splitLen:
        #print("add error %s for cont2"%(hit2.target))
        #print(h2_start, hdist)
        #print(hit2)
        err2=True              

    #print("RevRev")
    #print(err1, err2)
    #print(hit1)
    #print(h1_end)
    #print(hit2)
    #print(h2_start)
    #print("\t%i\n"%(cdist))
    return cdist, err1, err2

# function to calculate the distance between to contigs
# if both splits map on fwd strand
def calcHitContig_Dist_pos(hit1, hit2):
    err1=False
    err2=False
    h1_end = hit1.t_len -  (hit1.t_end + (hit1.q_len - hit1.q_end))
    h2_start = hit2.t_start - hit2.q_start
    # hdist->distance between splits
    if hit1.split != hit2.split:
        hdist = ((hit2.split - hit1.split - 1) * hit1.q_len)
    else:
        hdist = 0
    # calc contig distance
    cdist = hdist - (h1_end + h2_start)
    if h1_end > hdist and h1_end > globalVars.splitLen:
        #print("add error %s for cont1"%(hit1.target))
        #print(h1_end, hdist)
        #print(hit1)
        err1=True
    if h2_start > hdist and h2_start > globalVars.splitLen:
        #print(h2_start, hdist)
        #print("add error %s for cont2"%(hit2.target))
        #print(hit2)
        err2=True              

    #print("PosPos")
    #print(err1, err2)
    #print(hit1)
    #print(h1_end)
    #print(hit2)
    #print(h2_start)
    #print("\t%i\n"%(cdist))
    return cdist, err1, err2

# function to calculate the distance between to contigs
# if hit1 on fwd strand and hit2 on rev strand
def calcHitContig_Distance_posrev(hit1, hit2):
    err1=False
    err2=False
    h1_end = hit1.t_len -  (hit1.t_end + (hit1.q_len - hit1.q_end))
    h2_start = hit2.t_len - (hit2.t_end + hit2.q_start)
    # hdist->distance between splits
    if hit1.split != hit2.split:
        hdist = ((hit2.split - hit1.split - 1) * hit1.q_len)
    else:
        hdist = 0
    # calc contig distance
    cdist = hdist - (h1_end + h2_start)
    if h1_end > hdist and h1_end > globalVars.splitLen:
        #print("add error %s for cont1"%(hit1.target))
        #print(h1_end, hdist)
        #print(hit1)
        err1=True
    if h2_start > hdist and h2_start > globalVars.splitLen:
        #print("add error %s for cont2"%(hit2.target))
        #print(h2_start, hdist)
        #print(hit2)
        err2=True  

    #print("PosRev")
    #print(err1, err2)
    #print(hit1)
    #print(h1_end)
    #print(hit2)
    #print(h2_start)
    #print("\t%i\n"%(cdist))
    return cdist, err1, err2

# function to calculate the distance between to contigs
# if hit1 on rev strand and hit2 on fwd strand
def calcHitContig_Distance_revpos(hit1, hit2):
    err1=False
    err2=False
    h1_end = hit1.t_start - (hit1.q_len - hit1.q_end)
    h2_start = hit2.t_start - hit2.q_start
    # hdist->distance between splits
    if hit1.split != hit2.split:
        hdist = ((hit2.split - hit1.split - 1) * hit1.q_len)
    else:
        hdist = 0
    # calc contig distance
    cdist = hdist - (h1_end + h2_start)
    if h1_end > hdist and h1_end > globalVars.splitLen:
        #print("add error %s for cont1"%(hit1.target))
        #print(h1_end, hdist)
        #print(hit1)
        err1=True
    if h2_start > hdist and h2_start > globalVars.splitLen:
        #print("add error %s for cont2"%(hit2.target))
        #print(h2_start, hdist)
        #print(hit2)
        err2=True  
    
    #print("RevPos")
    #print(err1, err2)
    #print(hit1)
    #print(h1_end)
    #print(hit2)
    #print(h2_start)
    #print("\t%i\n"%(cdist))
    return cdist, err1, err2


# compares 2 hits on different contigs --> adds edges
def connectCONTS(prevhit, curhit, edgeHash):
    #print("running connectCONTS")
    #print(prevhit)
    #print(curhit)
    #if alns not at borders add errors for breaking of misassemblies
    errors=[]
    if prevhit.strand == '+' and curhit.strand == '+':
        # nodes to set connections from oriHA
        startNode = prevhit.target + "_1"
        endNode = curhit.target + "_0"
        hDist, e1, e2 = calcHitContig_Dist_pos(prevhit, curhit)
        #curError1=(prevhit.target, max(0,prevhit.t_end-100), min(prevhit.t_len-1, prevhit.t_end+100))
        #curError2=(curhit.target, max(0,curhit.t_start-100), min(curhit.t_len-1,curhit.t_start+100))
        curError1=(prevhit.target, prevhit.t_end+(prevhit.q_len-prevhit.q_end), prevhit.t_end+(prevhit.q_len-prevhit.q_end))
        curError2=(curhit.target, curhit.t_start-curhit.q_start, curhit.t_start-curhit.q_start)

    elif prevhit.strand == '-' and curhit.strand == '-':
        # nodes to set connections from
        startNode = prevhit.target + "_0"
        endNode = curhit.target + "_1"
        hDist, e1, e2 = calcHitContig_Dist_rev(prevhit, curhit)
        #curError1=(prevhit.target, max(0,prevhit.t_start-100), min(prevhit.t_len-1,prevhit.t_start+100))
        #curError2=(curhit.target, max(0,curhit.t_end-100), min(curhit.t_len-1,curhit.t_end+100))
        curError1=(prevhit.target, prevhit.t_start+(prevhit.q_start), prevhit.t_start+(prevhit.q_start))
        curError2=(curhit.target, curhit.t_end+(prevhit.q_start), curhit.t_end+(prevhit.q_start))

    elif prevhit.strand == '+' and curhit.strand == '-':
        # nodes to set connections from
        startNode = prevhit.target + "_1"
        endNode = curhit.target + "_1"
        hDist, e1, e2 = calcHitContig_Distance_posrev(prevhit, curhit)
        #curError1=(prevhit.target, max(0,prevhit.t_end-100), min(prevhit.t_len-1,prevhit.t_end+100))
        #curError2=(curhit.target, max(0,curhit.t_end-100), min(curhit.t_len-1,curhit.t_end+100))
        curError1=(prevhit.target, prevhit.t_end+(prevhit.q_len-prevhit.q_end), prevhit.t_end+(prevhit.q_len-prevhit.q_end))
        curError2=(curhit.target, curhit.t_end+(prevhit.q_start), curhit.t_end+(prevhit.q_start))

    elif prevhit.strand == '-' and curhit.strand == '+':
        # nodes to set connections from
        startNode = prevhit.target + "_0"
        endNode = curhit.target + "_0"
        hDist, e1, e2 = calcHitContig_Distance_revpos(prevhit, curhit)
        #curError1=(prevhit.target, max(0,prevhit.t_start-100), min(prevhit.t_len-1,prevhit.t_start+100))
        #curError2=(curhit.target, max(0,curhit.t_start-100), min(curhit.t_len-1,curhit.t_start+100))
        curError1=(prevhit.target, prevhit.t_start+(prevhit.q_start), prevhit.t_start+(prevhit.q_start))
        curError2=(curhit.target, curhit.t_start-curhit.q_start, curhit.t_start-curhit.q_start)

    #if contigs had long overlap left 
    #if e1==True and prevhit.split==curhit.split-1:
    if e1==True:
        #print("APPEND MISJOIN", curError1)
        #print(prevhit)
        #print(curhit)
        errors.append(curError1)
    #if e2==True and prevhit.split==curhit.split-1:
    if e2==True:
        #print("APPEND MISJOIN", curError2)
        #print(prevhit)
        #print(curhit)
        errors.append(curError2)
    # sort both nodes lexicographically to not have 2 different edges between
    # the same nodes e.g. one a->b and b->a in the dict
    if startNode < endNode:
        node1 = startNode
        node2 = endNode
    else:
        node1 = endNode
        node2 = startNode


    # add edge to "first" node
    if not node1 in edgeHash:
        edgeHash[node1] = {}
        edgeHash[node1][node2] = [hDist]
        #print("added edge with weight %i for conts %s and %s"%(hDist, node1, node2))
        #save readID spanning the gap
        globalVars.gapDICT[node1]={}
        globalVars.gapDICT[node1][node2]=[curhit.query]
        #print("Read %s connects conts %s and %s with dist %i"%(curhit.query, node1, node2, hDist))
        #print(edgeHash[node1][node2])
    else:
        if not node2 in edgeHash[node1]:
            edgeHash[node1][node2] = [hDist]
            #print("added edge with weight %i for conts %s and %s"%(hDist, node1, node2))
            #save readID spanning the gap
            globalVars.gapDICT[node1][node2]=[curhit.query]
            #print("Read %s connects conts %s and %s with dist %i"%(curhit.query, node1, node2, hDist))
            #print(edgeHash[node1][node2])
        else:
            edgeHash[node1][node2].append(hDist)
            #print("added edge with weight %i for conts %s and %s"%(hDist, node1, node2))
            #save readID spanning the gap
            globalVars.gapDICT[node1][node2].append(curhit.query)
            #print("Read %s connects conts %s and %s with dist %i"%(curhit.query, node1, node2, hDist))
            #print(edgeHash[node1][node2])
    return errors


# general function to loop over each hit list and compare each split to
# the next one --> calls connectCONTs and checkCONT
def checkHitlist(hitlist, edgeGRAPH, oriHASH):
    lastHit = ""
    # compare 2 hits following each other
    for curHits in hitlist:

        #if len(curHits) > 1 the hit maps repetitive ignore it
        if len(curHits)==1:           
            curHit=curHits[0]
            #globalVars.goodE[curHit.target].append((curHit.target, min(curHit.t_start, curHit.t_end), max(curHit.t_start, curHit.t_end)))
            #compare two hits
            if lastHit!="":
                #print("Comparing 2 hits:")
                #print(lastHit)
                #print(curHit)

                # check hits on same contig --> e.g. continuity
                if curHit.target == lastHit.target:
                    if curHit.strand == lastHit.strand:
                        #print("Check for INS/DEL")
                        # check for inseinsertErrorrtions and deletions
                        cError, cGood= checkINSDELS(lastHit, curHit)
                        #print("GOOD:", cGood)
                        #print("BAD:", cError)
                            
                        if cError!=None:
                            if cError[3] > 0:  
                                globalVars.insE[cError[0]].append(cError)
                            else:
                                globalVars.delE[cError[0]].append(cError)
                        else:
                            globalVars.goodE[curHit.target].append((curHit.target, cGood[0], cGood[1]))

                    else:
                        #print("Check for INV")
                        # add inversions
                        cError=checkINVS(lastHit, curHit, oriHASH)
                        if cError!=None:
                            globalVars.invE[cError[0]].append(cError)

                # check contig connections --> add edges to graph
                else:
                    #print("Check for CONTIG CONNECTION\n")
                    cErrors=connectCONTS(lastHit, curHit, edgeGRAPH)
                    for e in cErrors:
                        #e[2]+=100
                        globalVars.mjE[e[0]].append(e)
                     
            lastHit=curHit

    return edgeGRAPH