import globalVars
import merging
import copy

##################################
##################################
######functions###################
##################################
##################################


# function to set edges in graph
def setEdges(setEdge, agraph):
    if setEdge.direct!="":
        newEdgeStart = globalVars.myEdge(setEdge.start, setEdge.target,
                            setEdge.weight, setEdge.dist, setEdge.stdev, setEdge.direct)
        newEdgeTarget = globalVars.myEdge(setEdge.target, setEdge.start,
                            setEdge.weight, setEdge.dist, setEdge.stdev, setEdge.direct)
    else:
        newEdgeStart=""
        newEdgeTarget=""
        #print(setEdge)
    #print("\tLooks good --> Setting edge: %s" % (newEdgeStart))
    #print("\tLooks good --> Setting edge: %s" % (newEdgeTarget))

    oldEdge1=copy.copy(agraph.cGraph[setEdge.start].active)
    oldEdge2=copy.copy(agraph.cGraph[setEdge.target].active)
    if oldEdge1=="":
        oldEdge1=globalVars.myEdge(setEdge.start, setEdge.target, 0, 0, 0, "")
    if oldEdge2=="":
        oldEdge2=globalVars.myEdge(setEdge.target, setEdge.start, 0, 0, 0, "")
    agraph.cGraph[setEdge.start].active = newEdgeStart
    agraph.cGraph[setEdge.target].active = newEdgeTarget
    oldEdge=[oldEdge1, oldEdge2]
    #print("OLD EDGE")
    #print(oldEdge[0])
    #print(oldEdge[1])
    return oldEdge

# function to reset an old edge
def resetEdge(setEdge, agraph):
    #print("resetting edge")
    #print(setEdge)
    if setEdge.direct!="":
        newEdgeStart = globalVars.myEdge(setEdge.start, setEdge.target,
                            setEdge.weight, setEdge.dist, setEdge.stdev, setEdge.direct)
    else:
        newEdgeStart=""
    
    agraph.cGraph[setEdge.start].active = newEdgeStart

#function to adjust the gap dictionary in case  
def adjustGAPdict(new1, new2, old1, old2):

    #print("Adjusting gap dict from %s to %s by taking them from %s to %s"%(new1, new2, old1, old2))

    if old1 < old2:
        n1 = old1
        n2 = old2
    else:
        n1 = old2
        n2 = old1
    #print(n1, n2)
    gapReads=globalVars.gapDICT[n1][n2]

    if new1 < new2:
        node1 = new1
        node2 = new2
    else:
        node1 = new2
        node2 = new1
    if globalVars.gapDICT.get(node1, "nope")=="nope":
        globalVars.gapDICT[node1]={}
        globalVars.gapDICT[node1][node2]=[]
    elif globalVars.gapDICT[node1].get(node2, "nope")=="nope":
        globalVars.gapDICT[node1][node2]=gapReads
    else:
        globalVars.gapDICT[node1][node2]+=gapReads

# function to properly remove key from a dictionary
def removeMergedNEW(rkey, newTar, addDist):

    #print("Removing %s from dict"%(rkey))
    #print("Now pointing to %s with dist %i"%(newTar, addDist))
    globalVars.adjustDICT[rkey]=(newTar, addDist)

# function to check for check for merging and calls the merging functions
# from merging.py
def checkMerge(curEdge, cHASH):
   #print(curEdge.dist, globalVars.minOverlap*-1)
    #print(curEdge.dist <= globalVars.minOverlap*-1)
    #print("\tContigs are apparently overlapping - check for overlap for possible merging")
    #check if merging is possible and get the real overlap
    merged, realOL, mAdjust = merging.runBLAST(curEdge.start, curEdge.target, curEdge.dist, True)           
    #if cant be merged still set edge with distance 0
    #print("\t"+str(merged))    
    if merged==True:
        #print("\tsucessfully merged")
        #print(curEdge.start, curEdge.target, curEdge.dist, realOL, curEdge.dist*(-1)-realOL*(-1))
        curEdge.dist=realOL
        
    else:
        if merged=="Removed":
            #removeMergedNEW(cHASH, mAdjust[0][:-2]+"_0", mAdjust[1], mAdjust[2])
            #removeMergedNEW(cHASH, mAdjust[0][:-2]+"_0", mAdjust[3], mAdjust[4])
            curEdge.dist=realOL
            #print("Cont %s was part of the other -> abort for now"%(mAdjust[0][:-2]))

    return merged, mAdjust

# function to merge paths as defined in the GPMA paper
def mergeNEW(nEdge, inGraph):
    graph=inGraph.cGraph
    sucess=True
    edgeList=[]
    removal=[]
   #print("Start Merging Paths")
    inPath=[]
    curEdge=copy.copy(nEdge)
    end=False
    zipper='r'
    #zip right
    firstL=False
    while end==False:
        #print("while")
        #print(curEdge)     
        if curEdge.start[:-2] == curEdge.target[:-2]:
            #print("\tEdge with itself - skip")
            end=True
            break
       
        #if original edge is supposed to overlap contigs check for overlap first
        #abort if overlap not there
        if curEdge.dist < 0:
            merged, eAdjust=checkMerge(curEdge, inGraph)
            if merged==False:
                if curEdge.dist > (-2)*globalVars.distError:
                    #print("Merging failed but distance within range --> set to 1N for now")
                    curEdge.dist=1                  
                else:
                    #print("Merging failed!!!")
                    sucess=False
                    return sucess, [], []
            if merged == "Removed":
                next1=copy.copy(graph[curEdge.start].active)
                next2=copy.copy(graph[curEdge.target].cEdge)
                next22=copy.copy(graph[next2.target].active)
                #edgeList.append(copy.copy(curEdge))
                if curEdge.dist*(-1) >= globalVars.contLenDICT[curEdge.target[:-2]]-20:
                    #print("Duplicate case1: contig %s is part of %s"%(curEdge.target, curEdge.start))
                    if next22=="":
                        #print("End of path reached")
                        removal.append(eAdjust)
                        if graph[curEdge.target].active!="":
                            curEdge=copy.copy(graph[curEdge.target].active)
                            #print("Trying to zipper left path from merged cont guide edge:")
                            #print(curEdge)
                        else:
                            #print("nothing left to merge done here")
                            break
                    else:
                        curEdge.start=curEdge.start
                        curEdge.target=next22.target
                        curEdge.dist=curEdge.dist + (next2.dist + next22.dist)
                        #print("Merged implied edge:")
                        #print(curEdge)                      
                        removal.append(eAdjust) 
                        #next iteration
                        continue
                elif curEdge.dist*(-1) >= globalVars.contLenDICT[curEdge.start[:-2]]-20:
                    #print("Duplicate case2: contig %s is part of %s"%(curEdge.start, curEdge.target))
                    if next1=="":
                        #print("End of path reached")
                        removal.append(eAdjust)
                        otherEnd = graph[curEdge.start].cEdge.target
                        if graph[otherEnd].active!="":
                            curEdge=copy.copy(graph[otherEnd].active)
                            #print("Trying to zipper left path from merged cont guide edge:")
                            #print(curEdge)
                        else:
                            #print("nothing left to merge done here")
                            break
                    else:
                        curEdge.start=next2.target
                        curEdge.target=next1.target
                        curEdge.dist=next1.dist - curEdge.dist - next2.dist
                        #print("Merged implied edge:")
                        #print(curEdge)
                        removal.append(eAdjust)
                        #next iteration
                        continue
                else:
                    print("WTF why removed???")
                    print(globalVars.contLenDICT[curEdge.start[:-2]])
                    print(globalVars.contLenDICT[curEdge.target[:-2]])
                    exit()

                #sucess=False
                #return sucess, []                 
                
        if firstL==True:
            #print("skip first left")
            next1 = globalVars.myEdge(edgeList[0].target, edgeList[0].start, edgeList[0].weight, edgeList[0].dist, edgeList[0].stdev, edgeList[0].direct)
            firstL=False
        else:
            next1=copy.copy(graph[curEdge.start].active)
        next2=copy.copy(graph[curEdge.target].cEdge)
        if next1!="":
            next11=copy.copy(graph[next1.target].cEdge)
            #print(next1)
            #print(next11)
            #print(next2)
            if curEdge.start == next1.start and curEdge.target == next1.target:
                if abs(curEdge.dist - next1.dist) < 3*curEdge.stdev: 
                    sucess=True
                    break
                else:
                    #print("Egde already exists but distance failed")
                    next1=""
                    sucess=False
                    break
        elif next1=="":
            #print("End of %s-path reached edge looks good to set"%(zipper))
            edgeList.append(curEdge)
            if zipper=='r':
                zipper='l'
                firstL=True
                curEdge=copy.copy(graph[nEdge.target].active)
                if curEdge!="":
                    #print("Checking left path starting with edge")
                    #print(curEdge)
                    continue
                else:
                    #print("Original edge had nothing left to merge")
                    #print(nEdge)
                    #print(curEdge)
                    end=True
                    break
            else:
                end=True
                break
                
        
        #case curEdge+contigEdge of p2 fits in next read edge of p1
        #case1
        #if next1.dist >= curEdge.dist and next1.dist > 0:
        if next1.dist >= curEdge.dist:
            #adjust gapHASH if new dist > 0
            if (next1.dist-(curEdge.dist+next2.dist)) > (-2)*globalVars.distError:
                #print("New dist %i > 0 adjust gapHASH"%(next1.dist-(curEdge.dist+next2.dist)))
                adjustGAPdict(next2.target, next1.target, next1.start, next1.target)           
            #add curEdge to list because it would be set if merging is possible 
            edgeList.append(copy.copy(curEdge))      
            #imply new edge
            curEdge.start=next2.target # flip target and start here to continue
            curEdge.target=next1.target # flip target and start here to continue
            curEdge.dist=next1.dist-(curEdge.dist+next2.dist)
            #print("\timplied edge1")
            #print(curEdge)
            continue
        
        #case readEdge+ next contigEdge of p1 fits in curEdge
        #case2
        #elif curEdge.dist > next1.dist and curEdge.dist > 0:
        elif curEdge.dist > next1.dist:      
            #adjust gapHASH if new dist > 0
            if (curEdge.dist-(next1.dist+next11.dist)) > (-2)*globalVars.distError:
                #print("New dist %i > 0 adjust gapHASH"%(curEdge.dist-(next1.dist+next11.dist)))
                adjustGAPdict(next11.target, curEdge.target, curEdge.start, curEdge.target)        
            #imply new edge
            curEdge.start=next11.target # no flip needed
            curEdge.target=curEdge.target # no flip needed
            curEdge.dist=curEdge.dist-(next1.dist+next11.dist)

            #print("\timplied edge2")
            #print(curEdge)
            continue

    return sucess, edgeList, removal

 # returns an alternating path of edges
def getNodePath(ggraph, anode):
    #print("Get node path")
    ePath=[]
    pHash=[]
    curDist=0
    startNode = ggraph.findStart(anode)
    curNode=startNode
    pHash.append(startNode)
    ePath.append(ggraph.cGraph[curNode].cEdge)
    curDist+=ggraph.cGraph[curNode].cEdge.dist
    curNode=ggraph.cGraph[curNode].cEdge.target
        
    while ggraph.cGraph[curNode].active != "" and curNode not in pHash:
        #print(curNode)
        pHash.append(curNode)
        ePath.append(ggraph.cGraph[curNode].active)
        curNode=ggraph.cGraph[curNode].active.target
        #print(curNode)
        pHash.append(curNode)
        ePath.append(ggraph.cGraph[curNode].cEdge)
        curNode=ggraph.cGraph[curNode].cEdge.target
        
    
    pHash.append(curNode)

    return ePath, pHash
    
# given list of edges this function returns the happy and
# unhappiness for all edges with both nodes in the path
# in regard to the currently selected path
def checkHappiness(pdict, cDICT):
    nH=0
    nUH=0
    #print("Checking happiness of path")
    checked=[]
    #print(len(pdict))
    for c1 in pdict:
        checked.append(c1)
        for c2 in pdict:
            #print(c1, c2)
            # sort both nodes lexicographically to not have 2 different edges between
            # the same nodes e.g. one a->b and b->a in the dict
            if c1 < c2:
                node1 = c1
                node2 = c2
            else:
                node1 = c2
                node2 = c1
            
            if not c2 in checked:
                if node1 in globalVars.edgeDICT:
                    if node2 in globalVars.edgeDICT[node1]:
                        dist=cDICT.returnDist(node1, node2)
                        #print("%s and %s both in list with dist: %i"%(node1, node2, dist))
                        curEdges=globalVars.edgeDICT[node1][node2]
                        #curEdges+=globalVars.edgeDICT[node2][node1]
                        for curEdge in curEdges:
                            if curEdge[0] >= globalVars.minLinks:
                                #print(node1, node2)
                                #print("comparing distance %i with edge"%(dist))
                                #print(curEdge)
                                if dist != False:
                                    #print("Orientations fit set distance is %i predicted distance %i"%(dist, curEdge[1]))
                                    if abs(curEdge[1] - dist) <= max(3*curEdge[2], 100): #check if within 3*stdev of bundeld edge
                                        #print("Distance fits increasing happy by %i"%(curEdge[0]))
                                        nH+=curEdge[0]
                                        continue
                                    else:
                                        #print("Distance does not fit increasing UNhappy by %i"%(curEdge[0]))
                                        nUH+=curEdge[0]
                                        continue
                                else:
                                    #print("Incorrect orientation increasing UNhappy by %i"%(curEdge[0]))
                                    nUH+=curEdge[0]
                                    continue           

    return nH, nUH


# main function to run gpma
def gpma(newEdge, inGraph):
    #print("gpma")
    inputEdge=copy.copy(newEdge)
    #print(newEdge)

    # check if one edge points to a merged contig --> in that case needs to be
    # adjusted
    # while loop to go through multiple adjustments
    while globalVars.adjustDICT.get(newEdge.start, "nope") != "nope":
        #print(inputEdge)
        #if globalVars.adjustDICT.get(newEdge.start, "nope") != "nope":
        newEdge.dist -= globalVars.adjustDICT[newEdge.start][1]
        newEdge.start = globalVars.adjustDICT[newEdge.start][0]
        #print(newEdge.start, newEdge.target, inputEdge.start, inputEdge.target)
        adjustGAPdict(newEdge.start, newEdge.target, inputEdge.start, inputEdge.target)

        #print("edge adjusted to merged edge")
        #print(newEdge)

    while globalVars.adjustDICT.get(newEdge.target, "nope") != "nope":
        #print(inputEdge)
        #if globalVars.adjustDICT.get(newEdge.target, "nope") != "nope":
        newEdge.dist -= globalVars.adjustDICT[newEdge.target][1]
        newEdge.target = globalVars.adjustDICT[newEdge.target][0]
        #print(newEdge.start, newEdge.target, inputEdge.start, inputEdge.target)
        adjustGAPdict(newEdge.start, newEdge.target, inputEdge.start, inputEdge.target)
        #print("edge adjusted to merged edge")
        #print(newEdge)


    inputEdge=copy.copy(newEdge)
    #start directions
    
    #print("Path1:")
    #ePath1, pHASH1 = inGraph.returnEgdePath(newEdge.start)
    ePath1, pHASH1 = getNodePath(inGraph, newEdge.start)    
    #print(len(pHASH1))
    #for ele in ePath1:
    #    print(ele)
    nHP1, nUHP1 = checkHappiness(pHASH1, inGraph)
    #print("Happiness p1: %i/%i(H/UH)"%(nHP1, nUHP1))

    #print("Path2:")
    #ePath2, pHASH2 = inGraph.returnEgdePath(newEdge.target)
    ePath2, pHASH2 = getNodePath(inGraph, newEdge.target)
    #print(len(pHASH2))
    #for ele in ePath2:
    #    print(ele)
    nHP2, nUHP2 = checkHappiness(pHASH2, inGraph)
    #print("Happiness p2: %i/%i(H/UH)"%(nHP2, nUHP2))
                
    result, setList, removed = mergeNEW(inputEdge, inGraph)
    
    if result==True:
        oldEdges=[]
        for edge in setList:
            oE=setEdges(edge, inGraph)
            oldEdges=oE+oldEdges

        #print("New path:")
        t=True
        for ele in removed:
            if ele[0][:-2] == newEdge.start[:-2]:
                t=False
        if t== True:
            newPath, newHASH = getNodePath(inGraph, newEdge.start)
        else:
            newPath, newHASH = getNodePath(inGraph, newEdge.target)
        #print(len(newHASH))
        #for ele in newPath:
        #    print(ele)
        newHP, newUHP = checkHappiness(newHASH, inGraph)
        
        
        #check happiness
        if newHP - (nHP1+nHP2) >= newUHP - (nUHP1+nUHP2):
            #print("GPMA VERY HAPPY")
            for remove in removed: 
                removeMergedNEW(remove[0][:-2]+"_0", remove[1], remove[2])
                removeMergedNEW(remove[0][:-2]+"_1", remove[3], remove[4])
        else:
            #print("GPMA UNHAPPY EDGE SHOULD NOT HAVE BEEN SET")
            #[print(e) for e in newPath]
            #print("Happiness NEW PATH: %i/%i(H/UH)"%(newHP, newUHP))
            #[print(e) for e in ePath1]
            #print("Happiness p1: %i/%i(H/UH)"%(nHP1, nUHP1))
            #[print(e) for e in ePath2]
            #print("Happiness p2: %i/%i(H/UH)"%(nHP2, nUHP2))
            for edge in oldEdges: # if GPMA unhappy reset old graph
                resetEdge(edge, inGraph)
    #else:
    #    print("GPMA FAILED!!!")


    
