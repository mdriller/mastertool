import globalVars
import gpma
import subprocess, datetime
try:
    from subprocess import DEVNULL  # py3k
except ImportError:
    import os  # for python2
    DEVNULL = open(os.devnull, 'wb')


#function to reverse complemet a sequence
def reverseComplement(seq):
    old = "ACGT"
    replace = "TGCA"
    return seq.upper().translate(str.maketrans(old, replace))[::-1]


# function to run a blastn comparison between to contigs
def runBLAST(id1, id2, eOL, checkOL):
 
    #print("Merging %s and %s with estimasted OL:%i" % (id1, id2, estOL))
    seq1=globalVars.contDICT[id1[:-2]]
    seq2=globalVars.contDICT[id2[:-2]]
    seq1_trimmed=seq1
    seq2_trimmed=seq2
    if eOL*-1 >= len(seq1):
        
        if id2.endswith("1"):
            seq2_trimmed=seq2[len(seq2)-eOL*-1 : (len(seq2)-eOL*-1)+len(seq1)]
        elif id2.endswith("0"):
            seq2_trimmed=seq2[(eOL*-1)-len(seq1) : eOL*-1]
    if eOL*-1 >= len(seq2):
        
        if id1.endswith("1"):
            seq1_trimmed=seq1[len(seq2)-eOL*-1 : (len(seq2)-eOL*-1)+len(seq2)]
        elif id1.endswith("0"):
            seq1_trimmed=seq1[(eOL*-1)-len(seq2) : eOL*-1]

    tmpDIR = globalVars.outDir

    #print(len(seq1), len(seq1_trimmed))
    #print(len(seq2), len(seq2_trimmed))
    oWriter = open(tmpDIR + "/tmp1.fa", "w")
    #oWriter.write(">%s\n%s\n" % (id1, seq1))
    oWriter.write(">%s\n%s\n" % (id1, seq1_trimmed))
    oWriter.close()
    oWriter = open(tmpDIR + "/tmp2.fa", "w")
    #oWriter.write(">%s\n%s\n" % (id2, seq2))
    oWriter.write(">%s\n%s\n" % (id2, seq2_trimmed))
    oWriter.close()

    # define blastn output columns
    outfmt = "'6 qseqid sseqid pident length qstart qend sstart send sstrand evalue bitscore'"
    stime = datetime.datetime.now()
    #nod threads --> 'num_threads' is currently ignored when 'subject' is specified.
    subprocess.call("blastn -word_size 15 -outfmt %s -subject %s -query %s -out %s" % (outfmt, tmpDIR + "/tmp1.fa", tmpDIR + "/tmp2.fa", tmpDIR + "/tmp.blast6"),
                    shell=True, stdout=DEVNULL, stderr=subprocess.STDOUT)
    etime = datetime.datetime.now()
    #print("Blast Done time: " + str(etime-stime) + " [h:min:sec:millisec]")
    #increase BLAST Counter
    globalVars.blastCounter+=1

    
    # length need to be this way around --> 2 first then 1 beacuse 1 was used
    # as reference
    sucess, rOL, mAdjust = tryMERGE(tmpDIR + "/tmp.blast6", id1, id2, len(seq1_trimmed), len(seq2_trimmed))

    #if (rOL*(-1)==len(seq1_trimmed)-10 or rOL*(-1)==len(seq2_trimmed)-10) and checkOL==True:
    #    print("Full overlap rerun with more sequence!!!")
    #    return runBLAST(id1, id2, eOL, False)

    return sucess, rOL, mAdjust


# function for merging contigs by reading the blastn output
def tryMERGE(blastPATH, cID1, cID2, len1, len2):
    blastReader = open(blastPATH)

    mergeAdjust=""
    #just a initialization beacuse it needs a return value
    alnLen=1

    alllines = blastReader.readlines()
    if len(alllines) == 0:
        #print("\tERROR no hit found - NO MERGING CAN BE DONE!!!")
        merged = False
    else:
        #print("Number of lines in blast comp=%i" %(len(alllines)))
        for line in alllines:
            #print(line)
            splitted = line.strip().split("\t")
            # s2 first beacause s1 was used as reference
            s2 = splitted[0]
            s1 = splitted[1]
            ident = float(splitted[2])
            alnLen = int(splitted[3])
            # IMPORTANT BLAST INDICES START WITH 1 NOT WITH 0 --> -1 for all of
            s2_start = int(splitted[4]) - 1
            s2_end = int(splitted[5]) - 1
            s1_start = int(splitted[6]) - 1
            s1_end = int(splitted[7]) - 1
            s1_strand = splitted[8]

            #print("\tc1:%s len=%i ostart=%i oend=%i" %
            #      (cID1, len1, s1_start, s1_end))
            #print("\tc2:%s len=%i ostart=%i oend=%i" %
            #      (cID2, len2, s2_start, s2_end))

            # to merge as input argument
            if alnLen >= globalVars.minOverlap and ident >= globalVars.minIdent:
                
                #first check if alnLen is equal to len of one contig which means its fully part of the other and can be removed
                if alnLen >= len1-10:
                    #print("Contig %s is fully part of contig %s and will be removed!!!"%(cID1, cID2))
                    if s1_start < s1_end:
                        if s2_start < s2_end:
                            nTar0=cID2[:-2]+"_0"
                            aDist0=s2_start
                            nTar1=cID2[:-2]+"_1"
                            aDist1=len2-s2_end
                        elif s2_start > s2_end:
                            nTar0=cID2[:-2]+"_1"
                            aDist0=len2-s2_end
                            nTar1=cID2[:-2]+"_0"
                            aDist1=s2_start                  
                    elif s1_start > s1_end:
                        if s2_start < s2_end:
                            nTar0=cID2[:-2]+"_1"
                            aDist0=len2-s2_end
                            nTar1=cID2[:-2]+"_0"
                            aDist1=s2_start   
                        elif s2_start > s2_end:
                            nTar0=cID2[:-2]+"_0"
                            aDist0=s2_start
                            nTar1=cID2[:-2]+"_1"
                            aDist1=len2-s2_end                    
                    
                    #removeMerged(globalVars.contigGR, cID1[:-2]+"_0", nTar0, aDist0)
                    #removeMerged(globalVars.contigGR, cID1[:-2]+"_1", nTar1, aDist1)
                    mergeAdjust=(cID1, nTar0, aDist0, nTar1, aDist1)                                  
                    merged="Removed"
                    break        

                elif alnLen >= len2-10:
                    #print("Contig %s is fully part of contig %s and will be removed!!!"%(cID2, cID1))
                    if s1_start < s1_end:
                        if s2_start < s2_end:
                            nTar0=cID1[:-2]+"_0"
                            aDist0=s1_start
                            nTar1=cID1[:-2]+"_1"
                            aDist1=len1-s1_end
                        elif s2_start > s2_end:
                            nTar0=cID1[:-2]+"_1"
                            aDist0=len1-s1_end
                            nTar1=cID1[:-2]+"_0"
                            aDist1=s1_start                  
                    elif s1_start > s1_end:
                        if s2_start < s2_end:
                            nTar0=cID1[:-2]+"_1"
                            aDist0=len1-s1_end
                            nTar1=cID1[:-2]+"_0"
                            aDist1=s1_start   
                        elif s2_start > s2_end:
                            nTar0=cID1[:-2]+"_0"
                            aDist0=s1_start
                            nTar1=cID1[:-2]+"_1"
                            aDist1=len1-s1_end                     
                    
                    #removeMerged(globalVars.contigGR, cID2[:-2]+"_0", nTar0, aDist0)
                    #removeMerged(globalVars.contigGR, cID2[:-2]+"_1", nTar1, aDist1)
                    mergeAdjust=(cID2, nTar0, aDist0, nTar1, aDist1) 
                    merged="Removed"
                    break


                # get orientations of contigs meaning which ends are overlapping
                ori1 = cID1[-1]
                ori2 = cID2[-1]

                # case1 c1_1 --> c2_0
                if ori1 == "1" and ori2 == "0":
                    # example: c1:ctg7180000370328_1 len=1919 ostart=1418 oend=1918
                    # example: c2:ctg7180000371159_0 len=5383 ostart=0 oend=500
                    # to make sure alns are at the borders
                    if s1_end + 10 >= len1 - 1 and s2_start - 10 <= 0:
                        newSeqID = cID1 + "__" + cID2
                        saveMerge(newSeqID, cID1, cID2, alnLen)
                        #print("\tGOOD MERGING POSSIBLE!!!")
                        merged = True
                    else:
                        #print("ALNS NOT AT THE BORDERS!!!")
                        merged = False

                # case2 c1_1 --> c2_1
                elif ori1 == "1" and ori2 == "1":
                    #print("MERGE CASE 1-1")
                    # example: c1:ctg7180000370630_1 len=5524 ostart=5523 oend=5458
                    # example: c2:jtg7180000371240f_7180000371241f_1 len=12170
                    # ostart=12104 oend=12169
                    if s1_start + 10 >= len1 - 1 and s2_end + 10 >= len2 - 1:
                        newSeqID = cID1 + "__" + cID2
                        saveMerge(newSeqID, cID1, cID2, alnLen)
                        #print("\tGOOD MERGING POSSIBLE!!!")
                        merged = True
                    else:
                        #print("ALNS NOT AT THE BORDERS!!!")
                        merged = False

                # case3 c1_0 --> c2_1
                elif ori1 == "0" and ori2 == "1":
                    # example: c1:ctg7180000370268_0 len=3867 ostart=0 oend=419
                    # example: c2:ctg7180000370715_1 len=8150 ostart=7730 oend=8149
                    if s2_end + 10 >= len2 - 1 and s1_start - 10 <= 0:
                        newSeqID = cID2 + "__" + cID1
                        saveMerge(newSeqID, cID1, cID2, alnLen)
                        #print("\tGOOD MERGING POSSIBLE!!!")
                        merged = True
                    else:
                        #print("ALNS NOT AT THE BORDERS!!!")
                        merged = False

                # case4 c1_0 --> c2_0
                elif ori1 == "0" and ori2 == "0":
                    # example:  c1:ctg7180000370227_0 len=1568 ostart=415 oend=0
                    # example:  c2:ctg7180000371034_0 len=1684 ostart=0 oend=415
                    if s1_end - 10 <= 0 and s2_start - 10 <= 0:
                        newSeqID = cID2 + "__" + cID1
                        saveMerge(newSeqID, cID1, cID2, alnLen)
                        #print("\tGOOD MERGING POSSIBLE!!!")
                        merged = True
                    else:
                        #print("ALNS NOT AT THE BORDERS!!!")
                        merged = False
                #else:
                    #print("WHATDE WEIRD CONTIG ENDS!!!")

                if merged == True:
                    #print("\tMERGING SUCESSFULL!!!")
                    #print(newSeqID)
                    #if merging sucessful no reason to check more lines break out of loop ignore last alns
                    break
                #else:
                    #print("\tMERGING FAILED!!!")
            else:
                #print("\tMERGING FAILED EITHER TOO LITTLE OVERLAP(%i) OR IDENTITY(%f) TOO LOW" % (
                #    alnLen, ident))
                merged = False

    blastReader.close()
    return merged, alnLen*-1, mergeAdjust

# function to save the coordinates of the exact overlap a contig
def saveMerge(nID, oID1, oID2, olap):
    if oID1<=oID2:
        combi=oID1+"__"+oID2
    else:
        combi=oID2+"__"+oID1
    
    globalVars.mergedDICT[combi]=(nID, olap)



# function to properly remove key from a dictionary
def removeMerged(rdict, rkey, newTar, addDist):
    print("Tyring to remove %s from dict"%(rkey))
    try:
        print("Removing %s from dict"%(rkey))
        #part to adjust existing edge
        existingEdge=rdict[rkey].active
        globalVars.adjustDICT[rkey]=(newTar, addDist)
        if existingEdge!="":
            #print("existing Edge")
            #print(existingEdge)
            existingTarget=existingEdge.target
            rdict[existingTarget].active=""
            #print("Update edge with newTarget %s and added dist +%i"%(newTar, addDist))
            adjustedEdge=existingEdge
            adjustedEdge.target=newTar
            adjustedEdge.dist-=addDist # must be sustracted to properly adjust
            #print("adjusted Edge")
            #print(adjustedEdge)
            #print("Rerun checkEdge fit with adjusted edge!")
            #gpma.checkEdgeFit(adjustedEdge, rdict)
            #gpma.gpma()
        #remove contig from hash
        del rdict[rkey]
        


    except KeyError:
        print("keyError  while removeing")
        pass


def fillGAPS(cID1, cID2, cLen1, cLen2, cSeq1, cSeq2, rID, rLen, rSeq):

    tmpDIR = globalVars.outDir
    oWriter = open(tmpDIR + "/tmp_cont1.fa", "w")
    oWriter.write(">%s\n%s\n" % (cID1, cSeq1))
    oWriter.close()
    oWriter = open(tmpDIR + "/tmp_cont2.fa", "w")
    oWriter.write(">%s\n%s\n" % (cID2, cSeq2))
    oWriter.close()
    oWriter = open(tmpDIR + "/tmp_read.fa", "w")
    oWriter.write(">%s\n%s\n" % (rID, rSeq))
    oWriter.close()

    # define blastn output columns
    outfmt = "'6 qseqid sseqid pident length qstart qend sstart send sstrand evalue bitscore'"
    #no threads --> 'num_threads' is currently ignored when 'subject' is specified.
    subprocess.call("blastn -word_size 15 -outfmt %s -subject %s -query %s -out %s" % (outfmt, tmpDIR + "/tmp_cont1.fa", tmpDIR + "/tmp_read.fa", tmpDIR + "/tmp1.blast6"),
                    shell=True, stdout=DEVNULL, stderr=subprocess.STDOUT)
    subprocess.call("blastn -word_size 15 -outfmt %s -subject %s -query %s -out %s" % (outfmt, tmpDIR + "/tmp_cont2.fa", tmpDIR + "/tmp_read.fa", tmpDIR + "/tmp2.blast6"),
                    shell=True, stdout=DEVNULL, stderr=subprocess.STDOUT)

    ori1=cID1[-1]
    ori2=cID2[-1] 

    
    #print("TRYING TO MERGE C1:%s AND READ:%s..."%(cID1, rID))
    blastReader = open(tmpDIR + "/tmp1.blast6")
    alllines = blastReader.readlines()
    if len(alllines) == 0:
        #print("\tERROR no hit found - NO C1 MERGING CAN BE DONE!!!!!!!")
        merge1 = False
    else:
        #print("Number of lines in blast comp=%i" %(len(alllines)))
        for line in alllines:
            merge1=False
            #print(line)
            splitted = line.strip().split("\t")
            rID = splitted[0]
            cID = splitted[1]
            ident = float(splitted[2])
            alnLen = int(splitted[3])
            # IMPORTANT BLAST INDICES START WITH 1 NOT WITH 0 --> -1 for all of
            r_start = int(splitted[4]) - 1
            r_end = int(splitted[5]) - 1
            c_start = int(splitted[6]) - 1
            c_end = int(splitted[7]) - 1
            c_strand1 = splitted[8]

            #print("\tcontig:%s len=%i ostart=%i oend=%i cstrand=%s" %
            #      (cID1, cLen1, c_start, c_end, c_strand1))
            #print("\tread:%s len=%i ostart=%i oend=%i" %
            #      (rID, rLen, r_start, r_end))
            
            if alnLen >= 0.3*globalVars.splitLen and ident >= 80:
                end=max(c_start, c_end)
                if end + 20 >= cLen1:
                    merge1=True
                    #print("\tGOOD C1 MERGING POSSIBLE FROM START!!!!!!!") 
                    #get read starting position for concatenation
                    if c_strand1=="minus":
                        readStart=r_start
                    else:
                        readStart=r_end
                    #print(line)
                    break
                
                #else:
                #    print("\tERROR C1 MERGING FAILED ALN NOT AT BORDERS OF CONT!!!!!!!")
            #else:
            #    print("\tC1 MERGING FAILED EITHER TOO LITTLE OVERLAP(%i) OR IDENTITY(%f) TOO LOW" % (
            #        alnLen, ident))

    # no need to test to merge C2 with read if C1 merge failed 
    if merge1==False:
        #print("GAP FILLING FAILED!!!!!")
        return False


    blastReader.close()

    #print("TRYING TO MERGE C2:%s AND READ:%s..."%(cID2, rID))
    blastReader = open(tmpDIR + "/tmp2.blast6")   
    alllines = blastReader.readlines()
    if len(alllines) == 0:
        #print("\tERROR no hit found - NO C2 MERGING CAN BE DONE!!!!!!!")
        merge2 = False
    else:
        #print("Number of lines in blast comp=%i" %(len(alllines)))
        for line in alllines:
            merge2=False
            #print(line)
            splitted = line.strip().split("\t")
            # s2 first beacause s1 was used as reference
            rID = splitted[0]
            cID = splitted[1]
            ident = float(splitted[2])
            alnLen = int(splitted[3])
            # IMPORTANT BLAST INDICES START WITH 1 NOT WITH 0 --> -1 for all of
            r_start = int(splitted[4]) - 1
            r_end = int(splitted[5]) - 1
            c_start = int(splitted[6]) - 1
            c_end = int(splitted[7]) - 1
            c_strand2 = splitted[8]

            #print("\tcontig:%s len=%i ostart=%i oend=%i cstrand=%s" %
            #      (cID2, cLen2, c_start, c_end, c_strand2))
            #print("\tread:%s len=%i ostart=%i oend=%i" %
            #      (rID, rLen, r_start, r_end))

            if alnLen >= 0.3*globalVars.splitLen and ident >= 80:
                start=min(c_start, c_end)
                if start-20 <= 0:
                    merge2=True
                    #print("\tGOOD C2 MERGING POSSIBLE FROM START!!!!!!!") 
                    #get read starting position for concatenation
                    if c_strand2=="minus":
                        readEnd=r_end
                    else:
                        readEnd=r_start
                    #print(line)
                    break          
                #else:
                #    print("\tERROR C2 MERGING FAILED ALN NOT AT BORDERS OF CONT!!!!!!!")
            #else:
            #    print("\tC2 MERGING FAILED EITHER TOO LITTLE OVERLAP(%i) OR IDENTITY(%f) TOO LOW" % (
            #        alnLen, ident))

    blastReader.close()


    if merge1==True and merge2==True:
        if c_strand1 != c_strand1:
            #print("Gap filling failed hits were in different directions")
            return False
        #print("FILLING GAP IS POSSIBLE!!!!!")
        #print("rStart:%i rEnd%i"%(readStart, readEnd))
        #newID=cID1+"_"+rID+"_"+cID2
        #newSeq=cSeq1+rSeq[readStart:readEnd]+cSeq2
        if readStart<=readEnd:
            return rSeq[readStart:readEnd]
        else:
            return reverseComplement(rSeq[readEnd:readStart])
    else:
        #print("GAP FILLING FAILED!!!!!")
        return False

